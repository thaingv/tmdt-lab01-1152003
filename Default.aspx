﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lab01-GIT-1152003</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <form id="form1" runat="server">
    <div class="information">
        <div class="profile-picture">
            <img src="/nvthai_1.jpg" />
        </div>
        <div class="profile-text">
            MSSV: 1152003</br>
            Họ và tên: Nguyễn Văn Thái<br />
            Email: nvthai.fr@gmail.com
        </div>
    </div>
    </form>
</body>
</html>
